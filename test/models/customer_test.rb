require 'test_helper'

class CustomerTest < ActiveSupport::TestCase
  def setup
    @customer = customers(:one)
  end

  test 'look nice as string' do
    assert_equal 'abc123 - Abc Customer', @customer.to_s
  end

  test 'name validation' do
    @customer.name = nil
    assert_not @customer.valid?, 'Customer without a name is valid.'
  end

  test 'number validation' do
    @customer.number = nil
    assert_not @customer.valid?, 'Customer without a number is valid.'
  end

  test 'number is unique' do
    @other = Customer.new number: @customer.number, name: 'Other'
    assert_not @other.valid?, 'Customer with existent number is valid.'
  end
end
