require 'test_helper'

class PurchaseTest < ActiveSupport::TestCase
  def setup
    @purchase = purchases(:one)
  end

  test 'validates price' do
    @purchase.price = nil
    @purchase.product = nil
    assert_not @purchase.valid?, 'Purchase without price nor product is valid'
  end

  test 'validates customer' do
    @purchase.customer = nil
    assert_not @purchase.valid?, 'Purchase without customer is valid'
  end

  test 'validates product' do
    @purchase.product = nil
    assert_not @purchase.valid?, 'Purchase without product is valid'
  end

  test 'validates branch' do
    @purchase.branch = nil
    assert_not @purchase.valid?, 'Purchase without branch is valid'
  end
end
