require 'test_helper'

class ReportsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get purchases" do
    get :purchases
    assert_response :success
  end

  test "should get weekly" do
    get :weekly
    assert_response :success
  end

end
