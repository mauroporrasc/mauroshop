class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :number, null: false
      t.string :name, null: false
      t.string :detail

      t.timestamps null: false

      t.index :number, unique: true
    end
  end
end
