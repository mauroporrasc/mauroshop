class ReportMailer < ApplicationMailer
  def weekly
    @report = Report.weekly
    mail to: Setting.weekly_report_email, subject: 'Weekly report'
  end
end
