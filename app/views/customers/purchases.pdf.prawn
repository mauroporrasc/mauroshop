prawn_document do |pdf|
  pdf.text 'MauroShop - Purchases Report'
  pdf.text "Printed on: #{Time.now.strftime('%Y/%m/%d')}"

  pdf.move_down 30

  rows = []

  # Customer info.
  rows << [
    { content: 'Customer:', font_style: :bold },
    { content: @customer.to_s, colspan: 3 }
  ]

  # Headers.
  rows << [
    { content: 'Product Id', font_style: :bold },
    { content: 'Name', font_style: :bold },
    { content: 'Branch', font_style: :bold },
    { content: 'Price', font_style: :bold, align: :right }
  ]

  # Body.
  @purchases.each do |p|
    rows << [
      p.product.id,
      p.product.name,
      p.branch.name,
      { content: number_to_currency(p.price), align: :right }
    ]
  end

  # Total.
  rows << [
    { content: 'Total', font_style: :bold, colspan: 3, align: :right },
    {
      content: number_to_currency(@total_purchases),
      font_style: :bold,
      align: :right
    }
  ]

  t = pdf.make_table(rows, width: 500)
  t.draw
end
