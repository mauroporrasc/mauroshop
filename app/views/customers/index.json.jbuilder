json.array!(@customers) do |customer|
  json.extract! customer, :id, :number, :name, :detail
  json.url customer_url(customer, format: :json)
end
