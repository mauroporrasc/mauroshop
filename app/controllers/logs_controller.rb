class LogsController < ApplicationController
  before_action :set_log, only: [:show]

  def index
    @logs = Log.all
  end

  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_log
      @log = Log.find(params[:id])
    end
end
