class ReportsController < ApplicationController
  def index
  end

  def purchases
    return unless request.post?

    customer_number = params[:customer_number].strip
    customer = Customer.where(number: customer_number).first
    if customer
      redirect_to customer_purchases_path(customer)
    else
      flash.now[:warning] = "There is no customer with number '#{customer_number}'"
    end
  end

  def weekly
    ReportMailer.weekly.deliver_later
    @weekly_report_email = Setting.weekly_report_email
  end
end
