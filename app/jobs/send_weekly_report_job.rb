class SendWeeklyReportJob

  def self.perform(*args)
    ReportMailer.weekly.deliver_now
  end

end
