class Purchase < ActiveRecord::Base
  include Loggable

  belongs_to :customer
  belongs_to :product
  belongs_to :branch

  validates :customer, presence: true
  validates :price, presence: true
  validates :product, presence: true
  validates :branch, presence: true

  before_validation do
    unless price
      self.price = product.price if product
    end
  end

  after_create do
    OrderDetail.create order: Order.create, purchase: self
  end
end
