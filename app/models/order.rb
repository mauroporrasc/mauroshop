class Order < ActiveRecord::Base
  include Loggable

  has_many :purchases, through: :order_details

  before_create do
    self.number = Setting.next_order_number
    Setting.next_order_number += 1
  end
end
