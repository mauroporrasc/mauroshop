class Customer < ActiveRecord::Base
  include Loggable

  has_many :purchases

  validates :number, presence: true, uniqueness: true
  validates :name, presence: true

  scope :not_void, -> { where.not(name: '(Void)') }

  def to_s
    "#{number} - #{name}"
  end
end
