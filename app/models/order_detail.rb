class OrderDetail < ActiveRecord::Base
  include Loggable

  belongs_to :order
  belongs_to :purchase
end
